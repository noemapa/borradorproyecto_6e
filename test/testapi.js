const mocha = require('mocha');
const chai = require('chai');
const chaihttp=require('chai-http');

chai.use(chaihttp);

var should =chai.should();
var server= require('../server.js');

describe("First unit test",
  function()
  {

    it('Test that Duckduckgo works', function (done) {
           chai.request('https://www.google.es')
            .get('/')
            .end(
                function(err, res)
                {
                  console.log("Request has finished");
                  //console.log(err);
                  //console.log(res);
                  res.should.have.status(200);
                  done();

                }
            )

        }

      )

  }

)

describe("Test de API USuarios",
  function()
  {

    it('Prueba que el API de Usarios responde', function (done)
    {
           chai.request('http://localhost:3000')
            .get('/apitechu/v1/hello')
            .end(
                function(err, res)
                {
                  console.log("Request has finished")  ;
                  //console.log(err);
                  //console.log(res);
                  res.should.have.status(200);
                  done();

                }
            )

        }

      ),
      it('Prueba que el API devuelve una lista de usuarios correcta', function (done) {
             chai.request('http://localhost:3000')
              .get('/apitechu/v1/users')
              .end(
                  function(err, res)
                  {
                    console.log("Request has finished")  ;
                    res.should.have.status(200);
                    res.body.users.should.be.a("array");
                    for (user of res.body.users)
                    {
                      user.should.have.property("password");
                      user.should.have.property("email");
                    }
                    
                    done();

                  }
              )

          }

        )

  }

)
