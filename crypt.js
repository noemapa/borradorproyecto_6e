const bcrypt = require('bcrypt');


function hash(data){
  console.log("Hasing data");
  return bcrypt.hashSync(data,10);
}


function checkPassword(sentPassword, userHashedPassword){
  console.log("Checking Password");
  return bcrypt.compareSync(sentPassword, userHashedPassword);
}

//Exportación
module.exports.hash=hash;
module.exports.checkPassword=checkPassword;
