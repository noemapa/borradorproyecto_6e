//Inicializar express
var express =require('express');
var app = express();
const bodyParser= require('body-parser');
app.use(bodyParser.json());

//Que el puerto sea la vairable de entrono process.env.PORT o 3000
var port=process.env.PORT || 3000;

//Enlace con las librerías que hemos creado
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');

//API de pruebas
app.listen(port);
console.log("El API esta escuchando en el puerto "+port);

app.get('/apitechu/v1/hello',

     function (req,res)
     {
       console.log(" GET /apitechu/v1/hello");
       res.send({"msg":"Hola desde mi POD cuarto intento"});
     }
);

app.post('/apitechu/v1/monstruo/:p1/:p2',
function (req,res){
  console.log("POST /apitechu/v1/monstruo/:p1/:p2");

  console.log("Parámetros");
  console.log(req.params);
  console.log("Query string");
  console.log(req.query);
  console.log("Headers");
  console.log(req.headers);
  console.log("Body");
  console.log(req.body);
  console.log(req.body.first_name);

}
);

//app.get('/apitechu/v1/users',

  //   function (req,res)
  //   {
    //   console.log(" GET /apitechu/v1/users");
       //Envia un fichero donde __dirnme te marcar que la raiz es el directorio donde esta arrancado el atom
       //res.sendFile('usuarios.json', {root:__dirname});

      // var users = require('./usuarios.json');
  //     res.send(users);
     //}
//);


//APIS BUENAS
app.get('/apitechu/v1/users', userController.getUsersV1);
app.post('/apitechu/v1/users',userController.createUserV1);
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

app.get('/apitechu/v2/users', userController.getUsersV2);
app.get("/apitechu/v2/users/:id",userController.getUsersIdV2);
app.post("/apitechu/v2/users",userController.createUserV2);

app.post('/apitechu/v1/login',authController.loginV1);
app.post('/apitechu/v1/logout',authController.logoutV1);
app.post('/apitechu/v2/login',authController.loginV2);
app.post('/apitechu/v2/logout/:id',authController.logoutV2);
