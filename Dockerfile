# Imagen origen
FROM node:6.0.0

# Carpeta raiz
WORKDIR /apitechu

#Copia de archivos de la carpeta local a apitechu
ADD . /apitechu


RUN npm install

#Exponer puerto que estamos utilizando
EXPOSE 3000


#Comando de inicialización que se ejecuta cuando se instancia el contenedor, si pusieramos "RUN npm intall" descargar node_modñules si lo hemos incluido en .dockerignore
CMD ["npm", "start"]
