const baseMLabUrl = "https://api.mlab.com/api/1/databases/apitechudare6ed/collections/";
const mLabAPIKey= "apiKey=y1PDzgOkqtwu3yRKRpw6UouCzc16Qj_A";
const requestJson = require('request-json');
const io = require('../io');
const crypt = require('../crypt');


function getUsersV1(req,res)
{
     console.log(" GET /apitechu/v1/users?$topValue&$countValue");
     var users = require('../usuarios.json');
     var usersOut = new Object();
     usersOut.users=users;

     var topValue= req.query.$top;
     var countValue= req.query.$count;
     console.log("Top vale "+topValue);
     console.log("Count vale "+countValue);
     if (topValue != null)
     {
       usersOut.users=users.slice(0,topValue);
    }
    if (countValue!=null && countValue=="true")
    {
      usersOut.count=users.length;
    }
       res.send(usersOut);
}


function createUserV1(req,res)
{
  console.log(" POST /apitechu/v1/users");
  console.log(req.headers);
  var newUser = {
     "first_name": req.headers.first_name,
     "last_name": req.headers.last_name,
     "email": req.headers.email,

  }

  var users = require('../usuarios.json');
  users.push(newUser);

  io.writeUserDataToFile(users);

 res.send({"msg":"Usuarios añadido con exito"});
}


function deleteUserV1(req,res)
{
  console.log(" DELETE /apitechu/v1/users/:id");

  var users= require('../usuarios.json');
  //users.splice(req.params.id -1, 1);
  var i=0;
  for (user of users){

    if (user.id ==  req.params.id){
        users.splice(i,1);
        io.writeUserDataToFile(users);
        console.log("Usuario "+req.params.id+" borrado");
        break;
    }
    i=i+1;
 }
   res.send({"msg":"Usuario "+req.params.id+" borrado con exito"});
}


function getUsersV2(req,res)
{
  console.log(" GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabUrl);
  console.log("Cliente Creado");
  httpClient.get("user?"+mLabAPIKey,
   function(err, resMLab, body){
     var response = !err ? body: {
       "msg": "Error obteniendo usuarios"
     }
     res.send(response);
   }
 );
}


function getUsersIdV2(req,res)
{
  console.log(" GET /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = '?q={"id":'+id+'}&';

  var httpClient = requestJson.createClient(baseMLabUrl);
  console.log("Se buscar por "+id);
  httpClient.get("user"+query+mLabAPIKey,
   function(err, resMLab, body){
     if (err) {
       var response = {"msg": "Error obteniendo usuarios"}

     res.status(500);
   }
   else {
     if (body.length >0) {
       var response = body[0];
     }
     else {
       var response={"msg": "Usuario no encontrado"}
       res.status(404);
     }
   }

     res.send(response);
   }
 );
}


function createUserV2(req,res)
{
  console.log(" POST /apitechu/v2/users");
  first_name= req.body.first_name;
  last_name = req.body.last_name;
  email=req.body.email;
  password=req.body.password;
  id=req.body.id;

  console.log(first_name+" "+last_name+" "+email+" "+id);
  var newUser ={
    "first_name":first_name,
    "last_name": last_name,
    "email": email,
    "password" : crypt.hash(password),
    "id":id,

  }
  var httpClient = requestJson.createClient(baseMLabUrl);
  httpClient.post("user?"+mLabAPIKey, newUser,
   function(err, resMLab, body){
     var response = "OK";
     if (err)
     {
       response = {"msg": "Error insertando el usuario"};
       res.status(409)
     }

     res.send(response);
   }
  );

}

//Bloque para exponer hacia afuera los métodos
module.exports.getUsersV1=getUsersV1;
module.exports.createUserV1=createUserV1;
module.exports.deleteUserV1=deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersIdV2 = getUsersIdV2;
module.exports.createUserV2=createUserV2;
