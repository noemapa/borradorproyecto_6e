const requestJson = require('request-json');
const io = require('../io');
const crypt = require('../crypt');
const baseMLabUrl = "https://api.mlab.com/api/1/databases/apitechudare6ed/collections/";
const mLabAPIKey= "apiKey=y1PDzgOkqtwu3yRKRpw6UouCzc16Qj_A";

// FUNCION LOGIN VERSIÓN 2 CON MLAB
function loginV2(req,res){
  console.log(" POST /apitechu/v2/login");
  console.log(req.body);
  var users= require('../usuarios.json');
  var emailValue = req.body.email;
  var passwordValue = req.body.password;
  var mensaje= {};
  var putBody='{"$set":{"logged":true}}';
  mensaje.message="Login Incorrecto";

  var query = '?q={"email":"'+emailValue+'"}&';
 console.log(query);

  var httpClient = requestJson.createClient(baseMLabUrl);
  //Buscamos el cliente por email
  httpClient.get("user"+query+mLabAPIKey,
   function(err, resMLab, body){
     if (err) {
       res.send(mensaje);

    }
    else
    {

      //Comparo el password
      if (body.length>0 && crypt.checkPassword(passwordValue,body[0].password))
      {
        var query = '?q={"id":'+body[0].id+'}&';
        //Hago el cambio en mongo
        httpClient.put("user"+query+mLabAPIKey, JSON.parse(putBody),
         function(errPut, resMLabPut, bodyPut){
           if (errPut) {
              res.status(500);
              res.send(mensaje);
            }
          else {
            mensaje.message="Login Correcto";
            mensaje.userId=body[0].id;
            res.send(mensaje);
          }
       }
     );

      }
      else {
        res.status(500);
        res.send(mensaje);
      }


    }

 }
 );

}

// FUNCION LOGOUT VERSIÓN 2 CON MLAB
function logoutV2(req,res){
  console.log(" POST /apitechu/v2/logout/:id");

  var users= require('../usuarios.json');

  var idValue = req.params.id;
  var mensaje= {};
  var putBody='{"$unset":{"logged":""}}';
  mensaje.message="Logout Incorrecto";

  var query = '?q={"id":'+idValue+'}&';
console.log(query);

  var httpClient = requestJson.createClient(baseMLabUrl);
  //Buscamos el cliente por email
  httpClient.get("user"+query+mLabAPIKey,
   function(err, resMLab, body){
     if (err) {
       res.send(mensaje);

    }
    else
    {
             console.log(body[0].logged);
      //Miro si está logado
      if (body.length>0 && body[0].logged)
      {

        var query = '?q={"id":'+idValue+'}&';
        //Hago el cambio en mongo
        httpClient.put("user"+query+mLabAPIKey, JSON.parse(putBody),
         function(errPut, resMLabPut, bodyPut){
           if (errPut) {
              res.status(500);
              res.send(mensaje);
            }
          else {
            mensaje.message="Logout Correcto";
            mensaje.userId=idValue;
            res.send(mensaje);
          }
       }
     );

      }
      else {
        res.status(500);
        res.send(mensaje);
      }


    }

 }
 );

}

// FUNCION LOGOUT VERSIÓN 1 CON FICHEROS

function logoutV1(req,res){
  console.log(" POST /apitechu/v1/logout");
  console.log(req.body);
  var users= require('../usuarios.json');
  var idValue = req.body.id;
  var mensaje= {};
  mensaje.message="Logout Incorrecto";

  for (user of users){

    if (user.id ==  idValue && user.logged == "true"){
        delete user.logged;
        io.writeUserDataToFile(users);
        mensaje.message="Logout Correcto";
        mensaje.userId=user.id;
        res.send(mensaje);
    }

 }
  res.send(mensaje);

}

// FUNCION LOGIN VERSIÓN 1 CON FICHEROS
function loginV1(req,res){
  console.log(" POST /apitechu/v1/login");
  console.log(req.body);
  var users= require('../usuarios.json');
  var emailValue = req.body.email;
  var passwordValue = req.body.password;
  var mensaje= {};
  mensaje.message="Login Incorrecto";
  for (user of users){

    if (user.email ==  emailValue && user.password == passwordValue){
        user.logged="true";
        io.writeUserDataToFile(users);
        mensaje.message="Login Correcto";
        mensaje.userId=user.id;
        res.send(mensaje);
    }

 }
  res.send(mensaje);
}



//EXPORTACIÓN DE FUNCIONALIDES
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
